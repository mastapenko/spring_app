package com.epam.akella266.model.repositories;

import com.epam.akella266.model.entities.Basket;
import com.epam.akella266.model.entities.Item;

import com.epam.akella266.model.entities.Order;
import com.epam.akella266.model.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Repository
public class ItemsRepository implements com.epam.akella266.model.repositories.Repository {

    private DataSource dataSource;

    private Basket basket;
    private User user;

    @Autowired
    public ItemsRepository(DataSource dataSource, User user, Basket basket){
        this.dataSource = dataSource;
        this.basket = basket;
        this.user = user;
    }

    @Override
    public List<Item> getItems(){
        List<Item> queryItems = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("select * from items");
            while(rs.next()){
                queryItems.add(new Item(
                        rs.getInt("id") + "",
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getDouble("cost"),
                        rs.getInt("count"),
                        rs.getString("thumbnail"),
                        rs.getString("categoryId")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return queryItems;
    }

    @Override
    public Item getItem(String itemId){
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement st = connection.prepareStatement("select * from items where id = ?");
            st.setString(1, itemId);
            ResultSet rs = st.executeQuery();
            if (rs.next())
                return new Item(
                        rs.getInt("id") + "",
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getDouble("cost"),
                        rs.getInt("count"),
                        rs.getString("thumbnail"),
                        rs.getString("categoryId")
                );
            else
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public void addToBucket(Item item) throws Exception {
        basket.add(item);
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement st = connection.prepareStatement("select * from items where id = ?");
            st.setInt(1, Integer.parseInt(item.getId()));
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                int count = rs.getInt("count");
                if(count <= 0) throw new Exception("Not enough items");
                else {
                    PreparedStatement decrItem = connection.prepareStatement("update items set count = count - 1 where id = ? and count > 0");
                    decrItem.setInt(1, Integer.parseInt(item.getId()));
                    decrItem.execute();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void removeFromBucket(Item item){
        basket.remove(item);
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement decrItem = connection.prepareStatement("update items set count = count + 1 where id = ? and count > 0");
            decrItem.setInt(1, Integer.parseInt(item.getId()));
            decrItem.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getCountItems(){return basket.getItems().size();}

    public User getUser(String id){
        return user;
    }

    public Basket getBasket(){return basket;}

    public void makeOrder(){
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement st = connection.prepareStatement("insert into orderes (date, all_cost, user_id) values (?,?,?)");
            st.setDate(1, new Date(Calendar.getInstance().getTimeInMillis()));
            st.setDouble(2, basket.getAllCost());
            st.setInt(3, Integer.parseInt(user.getId()));
            st.execute();

            ResultSet rs = st.executeQuery("SELECT LAST_INSERT_ID() as last from orderes");
            rs.next();
            long orderId = rs.getLong("last");
            PreparedStatement stItems = connection.prepareStatement("insert into items_orders (id_item, id_order) values (?,?)");
            for(Item item : basket.getItems()){
                stItems.setInt(1, Integer.parseInt(item.getId()));
                stItems.setInt(2, (int) orderId);
                stItems.execute();
            }
            basket.clear();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Order> getOrders(String userId){
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement st = connection.prepareStatement("select * from orderes where user_id = ?");
            st.setInt(1, Integer.parseInt(userId));

            ResultSet rs = st.executeQuery();

            List<Order> orders = new ArrayList<>();
            while(rs.next()){
                orders.add(new Order(
                        rs.getString("id"),
                        rs.getDate("date"),
                        rs.getDouble("all_cost"),
                        new ArrayList<>()
                ));
            }

            for(Order order : orders){
                rs = st.executeQuery("select * from items_orders inner join items on items.id = items_orders.id_item where items_orders.id_order = " + order.getId());
                while(rs.next()){
                    order.getItems().add(new Item(
                            rs.getInt("id") + "",
                            rs.getString("name"),
                            rs.getString("description"),
                            rs.getDouble("cost"),
                            rs.getInt("count"),
                            rs.getString("thumbnail"),
                            rs.getString("categoryId")
                    ));
                }
            }
            return orders;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
