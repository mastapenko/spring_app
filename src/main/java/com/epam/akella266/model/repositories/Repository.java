package com.epam.akella266.model.repositories;

import com.epam.akella266.model.entities.Basket;
import com.epam.akella266.model.entities.Item;
import com.epam.akella266.model.entities.Order;
import com.epam.akella266.model.entities.User;

import java.util.List;

public interface Repository {
    List<Item> getItems();
    Item getItem(String itemId);
    void addToBucket(Item item) throws Exception;
    void removeFromBucket(Item item);
    int getCountItems();
    User getUser(String id);
    Basket getBasket();
    void makeOrder();
    List<Order> getOrders(String userId);
}
