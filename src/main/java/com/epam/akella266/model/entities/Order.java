package com.epam.akella266.model.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

    private String id;
    private Date date;
    private Double allCost;
    private List<Item> items;

    public Order() {}

    public Order(String id, Date date, Double allCost, List<Item> items) {
        this.id = id;
        this.date = date;
        this.allCost = allCost;
        this.items = items;
    }

    public Order(String id, Date date, Double allCost) {
        this(id, date, allCost, new ArrayList<>());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAllCost() {
        return allCost;
    }

    public void setAllCost(Double allCost) {
        this.allCost = allCost;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
