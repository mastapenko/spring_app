package com.epam.akella266.model.entities;

import java.util.ArrayList;
import java.util.List;

public class Basket {

    private List<Item> items;
    private double allCost;
    private boolean isEmpty;

    public Basket() {
        items = new ArrayList<>();
        allCost = 0;
        isEmpty = true;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
        this.items.forEach(item -> allCost += item.getCost());
        this.isEmpty = true;
    }

    public void clear(){
        items.clear();
        allCost = 0;
    }

    public void add(Item item){
        if (isEmpty) isEmpty = false;
        this.items.add(item);
        this.allCost += item.getCost();
    }

    public void remove(Item item){
        this.items.remove(item);
        this.allCost -= item.getCost();
        if(items.isEmpty()) {
            isEmpty = true;
            allCost = 0;
        }
    }

    public double getAllCost() {
        return allCost;
    }

    public void setAllCost(double allCost) {
        this.allCost = allCost;
    }

    public boolean getIsEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }
}
