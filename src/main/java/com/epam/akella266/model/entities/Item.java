package com.epam.akella266.model.entities;

public class Item {

    private String id;
    private String name;
    private String description;
    private double cost;
    private int count;
    private String thumbnail;
    private String catogoryId;

    public Item() {}

    public Item(String id, String name, String description, double cost, int count, String thumbnail, String catogoryId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.count = count;
        this.thumbnail = thumbnail;
        this.catogoryId = catogoryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCatogoryId() {
        return catogoryId;
    }

    public void setCatogoryId(String catogoryId) {
        this.catogoryId = catogoryId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Item)) return false;
        Item c = (Item)obj;
        return id.equals(c.getId());
    }
}
