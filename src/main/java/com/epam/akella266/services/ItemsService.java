package com.epam.akella266.services;

import com.epam.akella266.model.entities.Basket;
import com.epam.akella266.model.entities.Item;
import com.epam.akella266.model.entities.Order;
import com.epam.akella266.model.entities.User;
import com.epam.akella266.model.repositories.ItemsRepository;
import com.epam.akella266.model.repositories.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ItemsService{

    private Repository itemsRepository;

    @Autowired
    public ItemsService(ItemsRepository itemsRepository) {
        this.itemsRepository = itemsRepository;
    }

    public List<Item> getProd(){
        return itemsRepository.getItems().stream().filter(item -> item.getCatogoryId().equals("0")).collect(Collectors.toList());
    }

    public List<Item> getProm(){
        return itemsRepository.getItems().stream().filter(item -> item.getCatogoryId().equals("1")).collect(Collectors.toList());
    }

    public Item getItem(String itemId){
        return itemsRepository.getItem(itemId);
    }

    public void addToBucket(String itemId) throws Exception {
        Item item = itemsRepository.getItem(itemId);
        itemsRepository.addToBucket(item);
    }

    public void removeFromBucket(String itemId){
        Item item = itemsRepository.getItem(itemId);
        itemsRepository.removeFromBucket(item);
    }

    public int getCountItems(){return itemsRepository.getCountItems();}

    public Basket getBasket(){return itemsRepository.getBasket();}

    public User getUser(String id){
        return itemsRepository.getUser(id);
    }

    public void makeOrder(){
        itemsRepository.makeOrder();
    }

    public List<Order> getOrders(String userId){
        return itemsRepository.getOrders(userId);
    }
}
