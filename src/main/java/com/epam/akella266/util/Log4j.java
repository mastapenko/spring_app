package com.epam.akella266.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4j {
    private static Logger logger = LogManager.getLogger();

    public static void i(String message){
        logger.info(message);
    }

    public static void e(String message){
        logger.error(message);
    }
}
