package com.epam.akella266.controller;

import com.epam.akella266.model.entities.Item;
import com.epam.akella266.services.ItemsService;
import com.epam.akella266.util.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class MainController {

    @Autowired
    private ItemsService itemsService;

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    @RequestMapping(value = {"/", "prod"},method = RequestMethod.GET)
    public ModelAndView prod(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("items", itemsService.getProd());
        modelAndView.addObject("countItems", itemsService.getCountItems());
        modelAndView.setViewName("index.html");
        return modelAndView;
    }

    @RequestMapping(value = "prom", method = RequestMethod.GET)
    public ModelAndView prom(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("items", itemsService.getProm());
        modelAndView.addObject("countItems", itemsService.getCountItems());
        modelAndView.setViewName("index.html");
        return modelAndView;
    }

    @RequestMapping(value = "/addToBucket/{id}", method = RequestMethod.GET)
    public String addToBucket(@PathVariable String id) throws Exception {
        itemsService.addToBucket(id);
        return "redirect:/";
    }

    @RequestMapping(value = "/removeFromBasket/{id}", method = RequestMethod.GET)
    public String removefromBasket(@PathVariable String id){
        itemsService.removeFromBucket(id);
        return "redirect:/basket";
    }

    @RequestMapping(value = "/countItems", method = RequestMethod.GET)
    public ModelMap getCountItems(ModelMap modelMap){
        modelMap.addAttribute("countItems", itemsService.getCountItems());
        return modelMap;
    }

    @RequestMapping(value = "/basket", method = RequestMethod.GET)
    public ModelAndView getBasket(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("basket", itemsService.getBasket());
        modelAndView.addObject("countItems", itemsService.getCountItems());
        modelAndView.setViewName("basket.html");
        return modelAndView;
    }

    @RequestMapping(value = "/makeOrder", method = RequestMethod.GET)
    public String makeOrder(){
        itemsService.makeOrder();
        return "redirect:/";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ModelAndView getProfile(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", itemsService.getUser("1"));
        modelAndView.addObject("orders", itemsService.getOrders("1"));
        modelAndView.setViewName("profile.html");
        return modelAndView;
    }

    @RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    public ModelAndView getDetails(@PathVariable String id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("item", itemsService.getItem(id));
        modelAndView.addObject("countItems", itemsService.getCountItems());
        modelAndView.setViewName("details.html");
        return modelAndView;
    }
}
