CREATE DATABASE  IF NOT EXISTS `shop` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `shop`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: shop
-- ------------------------------------------------------
-- Server version	5.5.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) NOT NULL,
  `cost` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `thumbnail` varchar(100) NOT NULL,
  `categoryId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'яблоки','мама мыла раму',3,225,'/static/images/apples.jpg',0),(2,'баклажан','мама мыла раму',5,350,'/static/images/baklagan.jpeg',0),(3,'картошка','мама мыла раму',50,500,'/static/images/bulba.jpg',0),(4,'доска','мама мыла раму',60,20,'/static/images/doska.jpg',1),(5,'гранат','мама мыла раму',8,3,'/static/images/granat.jpg',0),(6,'груша','мама мыла раму',4,200,'/static/images/grusha.jpeg',0),(7,'гвозди','мама мыла раму',10,499,'/static/images/gvozdi.jpg',1),(8,'капуста','мама мыла раму',5,99,'/static/images/kapusta.jpg',0),(9,'киви','мама мыла раму',2,50,'/static/images/kiwi.jpg',0),(10,'мандарины','мама мыла раму',5,200,'/static/images/mandariny.jpg',0),(11,'мел','мама мыла раму',3,1,'/static/images/mel.jpg',1),(12,'плита','мама мыла раму',26,30,'/static/images/plita.jpg',1),(13,'стол','мама мыла раму',150,14,'/static/images/stol.jpg',1),(14,'стул','мама мыла раму',50,50,'/static/images/stul.jpg',1),(15,'вата','мама мыла раму',1,1000,'/static/images/vata.jpg',1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_orders`
--

DROP TABLE IF EXISTS `items_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_orders`
--

LOCK TABLES `items_orders` WRITE;
/*!40000 ALTER TABLE `items_orders` DISABLE KEYS */;
INSERT INTO `items_orders` VALUES (1,3,1),(2,2,1),(3,7,2),(4,14,2),(5,7,3),(6,2,4),(7,8,4),(8,11,5),(9,13,5);
/*!40000 ALTER TABLE `items_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderes`
--

DROP TABLE IF EXISTS `orderes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `all_cost` float NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderes`
--

LOCK TABLES `orderes` WRITE;
/*!40000 ALTER TABLE `orderes` DISABLE KEYS */;
INSERT INTO `orderes` VALUES (1,'2018-09-14 00:00:00',55,1),(2,'2018-09-16 00:00:00',60,1),(3,'2018-09-16 00:00:00',10,1),(4,'2018-09-16 00:00:00',10,1),(5,'2018-09-16 00:00:00',153,1);
/*!40000 ALTER TABLE `orderes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-16 19:33:44
